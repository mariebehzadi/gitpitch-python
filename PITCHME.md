---?image=template/img/bg-2.png&size=cover
@title[Homepage]

@snap[west byline]
<h1> @color[#4584b6](Python) </h1>
<h3> @color[#041e47](Crash Course Workshop) </h3>
@snapend

@snap[south byline]
<a href="https://gitlab.com/behzadim/" target="_blank">@color[#5e2750](@fa[gitlab fa-1x] behzadim)</a>
@snapend


---?image=template/img/poster.png&position=right&size=30% 100%
@title[End]


---
@title[Slide Markdown]

### Each slide in this presentation is provided as a *template*.

<br><br>

1. Select only the slide templates that you need.
1. Customize the template _markdown content_.
1. Optionally, override template _styles_ and _settings_.
1. Then present and publish with GitPitch @fa[smile-o]
<br><br>


---
@title[Tip! Fullscreen]

![TIP](template/img/tip.png)
<br>
For the best viewing experience, press F for fullscreen.
@css[template-note](We recommend using the *SPACE* key to navigate between slides.)

---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/boxed-text/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---
@title[The Template Docs]

@snap[west span-100]
### **Now it's @color[#E49436](your) turn.**

<br>

#### Use these templates to create custom slides.
#### **Then amaze your audience with a Git@color[#E49436](Pitch) slideshow @fa[smile-o]**
@snapend

@snap[south docslink span-100]
For supporting documentation see the [The Template Docs](https://gitpitch.com/docs/the-template)
@snapend
